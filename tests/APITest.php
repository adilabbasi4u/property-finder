<?php

require_once __DIR__ . '/../vendor/autoload.php';

class APITest extends \PHPUnit\Framework\TestCase
{
	private $payload = '{
		"data": [{
			"source": "Barcelona",
			"destination": "Gerona Airport",
			"transportation" : "train P778",
			"details" : "Gate 22, seat A4. Baggage will be transferred to your last leg."
		}, {
			"source": "Madrid",
			"destination": "Barcelona",
			"transportation" : "train R987",
			"details" : "Gate 22, seat 7B."
		}]
	}';

	private $response = [
		"data" => [],
		"errors" => [
			"details" => "Invalid Request."
		]
	];

	public function testResponse()
	{
		$stub = $this->getMockBuilder(SortModule\API::class)
			->setMethods(['response'])
			->getMock();

		$stub->expects($this->once())
			->method('response')
			->with($this->response['data'], $this->response['errors'], 400)
			->will($this->returnValue(json_encode($this->response, JSON_FORCE_OBJECT)));

		$this->assertEquals(
			json_encode($this->response, JSON_FORCE_OBJECT),
			$stub->response($this->response['data'], $this->response['errors'], 400)
		);
    }
}
?>