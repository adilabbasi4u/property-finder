<?php

require_once __DIR__ . '/../vendor/autoload.php';

class TravellingTest extends \PHPUnit\Framework\TestCase
{
	private $payload = '{
		"data": [{
			"source": "Barcelona",
			"destination": "Gerona Airport",
			"transportation" : "train P778",
			"details" : "Gate 22, seat A4. Baggage will be transferred to your last leg."
		}, {
			"source": "Madrid",
			"destination": "Barcelona",
			"transportation" : "train R987",
			"details" : "Gate 22, seat 7B."
		}]
	}';

	private $request = [
		'data' => [
			0=> [
				"source" => "Barcelona",
				"destination" => "Gerona Airport",
				"transportation" => "train P778",
				"details" => "Gate 22, seat A4. Baggage will be transferred to your last leg."
			],
			1=> [
				"source" => "Madrid",
				"destination" => "Barcelona",
				"transportation" => "train R987",
				"details" => "Gate 22, seat 7B."
			]
		]
	];

	private $locations = [
		'Madrid' => [
			'source' => 'Madrid',
			'destination' => "Barcelona",
			'transportation' => "train R987",
			'details' => "Gate 22, seat 7B.",
			'count' => 1
		],
		'Barcelona' => [
			"source" => "Barcelona",
			"destination" => "Gerona Airport",
			"transportation" => "train P778",
			"details" => "Gate 22, seat A4. Baggage will be transferred to your last leg.",
			'count' => 2
		],
		'Gerona Airport' => [
			'count' => 1
		]
	];

	private $startAndEndPoints = [
		'Madrid' => [
			'source' => 'Madrid',
			'destination' => "Barcelona",
			'transportation' => "train R987",
			'details' => "Gate 22, seat 7B.",
			'count' => 1
		],
		'Gerona Airport' => [
			'count' => 1
		]
	];

	private $SortedBoardingData = [
		"data" => [
			0 => "Take train R987 from Madrid to Barcelona. Gate 22, seat 7B.",
			1 => "Take train P778 from Barcelona to Gerona Airport. Gate 22, seat A4. Baggage will be transferred to your last leg."
		],
		"errors" => []
	];

	public function testGetRequest()
	{
		$travelling = new SortModule\Travelling($this->payload);
		$this->assertEquals($this->request['data'], $travelling->getRequest());
	}

	public function testGenerateRequest()
	{
		$instance = new SortModule\Travelling($this->payload);
		$method = new \ReflectionMethod($instance, '_generateRequest');
		$method->setAccessible(true);
		$this->assertEquals(
			$this->request['data'],
			$method->invoke($instance, $this->payload)
		);
	}

	public function testGenerateStartEndLocations()
	{
		$instance = new SortModule\Travelling($this->payload);
		$method = new \ReflectionProperty(get_class($instance), 'locations');
		$method->setAccessible(true);
		$method->setValue($instance, $this->locations);

		$method = new \ReflectionProperty(get_class($instance), 'startAndEndPoints');
		$method->setAccessible(true);
		$method->setValue($instance, $this->startAndEndPoints);

		$method = new \ReflectionMethod($instance, '_generateStartEndLocations');
		$method->setAccessible(true);
		$this->assertEquals(
			[0 => 'Madrid', 1 => 'Gerona Airport'],
			$method->invoke($instance)
		);
	}

	public function testTrackStartAndEndPoints()
	{
		$instance = new SortModule\Travelling($this->payload);
		$method = new \ReflectionProperty(get_class($instance), 'locations');
		$method->setAccessible(true);
		$method->setValue($instance, $this->locations);

		$method = new \ReflectionProperty(get_class($instance), 'startAndEndPoints');
		$method->setAccessible(true);
		$method->setValue($instance, $this->startAndEndPoints);

		$method = new \ReflectionMethod($instance, '_trackStartAndEndPoints');
		$method->setAccessible(true);
		$this->assertEquals(
			['source' => true, 'destination' => true],
			$method->invoke($instance, 'Madrid', 'Gerona Airport')
		);
		$this->assertNotEquals(
			['source' => true, 'destination' => true],
			$method->invoke($instance, 'Madrid', 'Barcelona')
		);
	}

	public function testGenerateConnectionCount()
	{
		$instance = new SortModule\Travelling($this->payload);
		$method = new \ReflectionProperty(get_class($instance), 'locations');
		$method->setAccessible(true);
		$method->setValue($instance, $this->locations);

		$method = new \ReflectionMethod($instance, '_generateConnectionCount');
		$method->setAccessible(true);
		$this->assertEquals(2,$method->invoke($instance, 'Madrid'));
		$this->assertEquals(1,$method->invoke($instance, 'Not a place'));
	}

	public function testGenerateBoardingData()
	{
		$instance = new SortModule\Travelling($this->payload);
		$method = new \ReflectionProperty(get_class($instance), 'request');
		$method->setAccessible(true);
		$method->setValue($instance, $this->request['data']);

		$this->assertEquals($this->locations, $instance->generateBoardingData());
		$this->assertNotEquals($this->request['data'], $instance->generateBoardingData());
	}

	public function testGenerateSortedBoardingData()
	{
		$instance = new SortModule\Travelling($this->payload);
		$method = new \ReflectionProperty(get_class($instance), 'startLocation');
		$method->setAccessible(true);
		$method->setValue($instance, 'Madrid');

		$method = new \ReflectionProperty(get_class($instance), 'locations');
		$method->setAccessible(true);
		$method->setValue($instance, $this->locations);

		$this->assertEquals(
			json_encode($this->SortedBoardingData, JSON_FORCE_OBJECT),
			$instance->generateSortedBoardingData()
		);
	}
}

