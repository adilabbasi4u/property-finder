Dependencies:
------------
php version         -> PHP 5.6.27
Composer version    -> 1.2.1

These dependencies need to be installed already for sorting problem.



step 1: (install rest of dependencies)
--------------------------------------------
run >>      composer insall



step 2: (to generate endpoint documentation)
--------------------------------------------
run >>      bash documentation.sh
Note: documentation available in html at docs/api/index.html



step 3: (for unittest)
--------------------------------------------
run >>      vendor/bin/phpunit



step 4: (hit API via any REST CLIENT like Postman)
--------------------------------------------
- for example it is placed some where on the root of server directory
(www or www/html) in the folder "property-finder"

then you can hit endpoint like,

 HTTP   URL
[POST]  http://localhost/property-finder/sort.php

Request Body:
{
    "data": [{
        "source": "Barcelona's",
        "destination": "Gerona Airport",
        "transportation" : "train 123",
        "details" : " Gate 22, seat 7B."
    }, {
        "source": "Stockholm",
        "destination": "New York JFK",
        "transportation" : "train 1234",
        "details" : "Baggage will we automatically transferred from your last leg."
    }, {
        "source": "Gerona Airport",
        "destination": "Stockholm",
        "transportation" : "train 12345"
    }, {
        "source": "Madrid",
        "destination": "Barcelona's",
        "transportation" : "train 123456",
        "details" : " Gate 22, seat 7B."
    }]
}
----------------xxx---------------xxx------------

- Response will be generated with sorted data like,

{
  "data": {
    "0": "Take train 123456 from Madrid to Barcelona's. Gate 22, seat 7B.",
    "1": "Take train 123 from Barcelona's to Gerona Airport. Gate 22, seat 7B.",
    "2": "Take train 12345 from Gerona Airport to Stockholm. ",
    "3": "Take train 1234 from Stockholm to New York JFK. Baggage will we automatically transferred from your last leg."
  },
  "errors": {}
}


