<?php
	namespace SortModule;

	abstract class API
	{
		private function _setHeaders($status)
		{
			header("Access-Control-Allow-Orgin: *");
			header("Access-Control-Allow-Methods: *");
			header("Content-Type: application/json");
			header(http_response_code($status));
		}

		public function response($data=[], $errors=[], $status=200)
		{
			$this->_setHeaders($status);
			$response = ['data' => $data, 'errors' => $errors];
			return json_encode($response, JSON_FORCE_OBJECT);
		}
	}
