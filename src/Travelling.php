<?php
	namespace SortModule;

	class Travelling extends API
	{
		private $locations;
		private $startAndEndPoints;
		private $locationFrequency;
		private $startLocation;
		private $endLocation;
		private $request;

		public function __construct($request)
		{
			$this->locations = array();
			$this->startAndEndPoints = array();
			$this->locationFrequency = 1;
			$this->startLocation = null;
			$this->endLocation = null;
			$this->request = $this->_generateRequest($request);
		}

		public function getRequest()
		{
			return $this->request;
		}

		private function _generateRequest($request)
		{
			try{
				$request = json_decode($request, true);
			}
			catch (Exception $exception){
				// log exception
				return null;
			}

			if(isset($request['data']) && gettype($request['data']) === 'array'){
				return $request['data'];
			}

			return null;
		}

		public function generateBoardingData()
		{
			$records = $this->request;
			foreach ($records as $record)
			{
				$source = $record['source'];
				$destination = $record['destination'];

				$this->locations[$destination]['count'] = $this->_generateConnectionCount($destination);
				$record['count'] = $this->_generateConnectionCount($source);
				$this->locations[$source] = $record;
				$this->_trackStartAndEndPoints($source, $destination);
			}
			$this->_generateStartEndLocations();
			return $this->locations;
		}

		private function _generateConnectionCount($locationIndex)
		{
			if(isset($this->locations[$locationIndex]['count'])){
				return $this->locations[$locationIndex]['count'] + 1;
			}

			return 1;
		}

		private function _trackStartAndEndPoints($source, $destination)
		{
			$this->startAndEndPoints[$source] = $this->locations[$source]['count'];
			$this->startAndEndPoints[$destination] = $this->locations[$destination]['count'];

			if($this->locations[$source]['count'] > $this->locationFrequency){
				unset($this->startAndEndPoints[$source]);
			}

			if ($this->locations[$destination]['count'] > $this->locationFrequency){
				unset($this->startAndEndPoints[$destination]);
			}

			return [
				"source" => isset($this->startAndEndPoints[$source]),
				"destination" => isset($this->startAndEndPoints[$destination])
			];
		}

		public function generateSortedBoardingData()
		{
			$location = $this->startLocation;

			foreach($this->locations as $locations => $details)
			{
				if(isset($this->locations[$location]['destination']))
				{
					$sortedBoardings[] = sprintf("Take %s from %s to %s. %s",
						$this->locations[$location]['transportation'],
						$this->locations[$location]['source'],
						$this->locations[$location]['destination'],
						ltrim($this->locations[$location]['details'])
					);

					$location = $this->locations[$location]['destination'];
				}

			}
			return $this->response($sortedBoardings);
		}

		private function _generateStartEndLocations()
		{
			foreach($this->startAndEndPoints as $location => $frequency)
			{
				if (isset($this->locations[$location]['source'])){
					$this->startLocation = $location;
				}
				else{
					$this->endLocation = $location;
				}
			}

			return [$this->startLocation, $this->endLocation];
		}
	}
