<?php
	require_once __DIR__ . '/vendor/autoload.php';
	use \SortModule\Travelling;

	/**
	* Sort boading cards
	*
	* Takes input the un-sorted list of boarding cards and sorts them.
	*
	* @Details
	* @HTTP [POST]
	* @EndPoint .../sort.php
	* @Request
		{
			"data": [{
				"source": "Barcelona's",
				"destination": "Gerona Airport",
				"transportation" : "train 123",
				"details" : " Gate 22, seat 7B."
			}, {
				"source": "Stockholm",
				"destination": "New York JFK",
				"transportation" : "train 1234",
				"details" : "Baggage will we automatically transferred from your last leg."
			}, {
				"source": "Gerona Airport",
				"destination": "Stockholm",
				"transportation" : "train 12345"
			}, {
				"source": "Madrid",
				"destination": "Barcelona's",
				"transportation" : "train 123456",
				"details" : " Gate 22, seat 7B."
			}]
		}
	* @apiGroup Travelling
	* @apiName Sort Boading Cards
	* @apiDescription Sort un-ordered boarding cards for travelling
	* @apiParam {array} [data] Array of objects with
	* @response
		{
			"data": {
				"0": "Take train 123456 from Madrid to Barcelona's. Gate 22, seat 7B.",
				"1": "Take train 123 from Barcelona's to Gerona Airport. Gate 22, seat 7B.",
				"2": "Take train 12345 from Gerona Airport to Stockholm. ",
				"3": "Take train 1234 from Stockholm to New York JFK. Baggage will we automatically transferred from your last leg."
			},
			"errors": {}
		}
	* @return
		{
			"data": {
				"0": "Take train 123456 from Madrid to Barcelona's. Gate 22, seat 7B.",
				"1": "Take train 123 from Barcelona's to Gerona Airport. Gate 22, seat 7B.",
				"2": "Take train 12345 from Gerona Airport to Stockholm. ",
				"3": "Take train 1234 from Stockholm to New York JFK. Baggage will we automatically transferred from your last leg."
			},
			"errors": {}
		}
	*/
	function boardingSort($request)
	{
		$trip = New Travelling($request);
		if(empty($trip->getRequest())){
			return $trip->response([], ['details' => 'Invalid Request.'], 400);
		}

		$trip->generateBoardingData();
		return $trip->generateSortedBoardingData();
	}

	if ($_SERVER['REQUEST_METHOD'] === 'POST'){
		echo boardingSort(file_get_contents('php://input'));
		return;
	}

	$trip = New Travelling('');
	echo $trip->response([], ['details' => 'Request Not Found.'], 404);


?>